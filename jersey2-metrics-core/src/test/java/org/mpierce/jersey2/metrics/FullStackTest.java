package org.mpierce.jersey2.metrics;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Timer;
import com.ning.http.client.Response;
import java.io.IOException;
import java.util.Map;
import java.util.SortedMap;
import java.util.concurrent.ExecutionException;
import javax.annotation.Nonnull;
import org.junit.Test;
import org.mpierce.jersey2.metrics.test.AbstractMetricsTestBase;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mpierce.jersey2.metrics.test.SlowResource.SLEEP_TIME;

public final class FullStackTest extends AbstractMetricsTestBase {

    @Test
    public void testMakeRequestToBasicResource() throws ExecutionException, InterruptedException {
        assertEquals(200, req("/resource").getStatusCode());

        waitForTimers(10);

        Map.Entry<String, Timer> timerEntry = registry.getTimers().entrySet().stream()
                .filter(e -> e.getValue().getCount() > 0).findFirst().get();

        assertEquals(1, timerEntry.getValue().getCount());

        Map.Entry<String, Counter> counterEntry = registry.getCounters().entrySet().stream()
                .filter(e -> e.getValue().getCount() > 0).findFirst().get();

        assertEquals(1, counterEntry.getValue().getCount());
    }

    @Test
    public void testMakeRequestToSubresource() throws ExecutionException, InterruptedException {
        assertEquals(200, req("/resourceWithSubresource/sub").getStatusCode());
    }

    @Test
    public void testMakeRequestToSubresourceLocatorWithoutSubresource() throws ExecutionException,
            InterruptedException {

        assertEquals(200, req("/resourceWithSubResourceLocators/srlWithResource").getStatusCode());
    }

    @Test
    public void testMakeRequestToSuperclass() throws ExecutionException, InterruptedException {
        assertEquals(200, req("/parent").getStatusCode());

        assertTimerPresent("/parent.method=GET.timer");
    }

    @Test
    public void testMakeRequestToSubclass() throws ExecutionException, InterruptedException {
        assertEquals(200, req("/child").getStatusCode());

        assertTimerPresent("/child.method=GET.timer");
    }

    @Test
    public void testSlowResourceIsMeasuredAsSlow() throws ExecutionException, InterruptedException, IOException {
        Response resp = req("/slow");
        assertEquals(200, resp.getStatusCode());
        assertEquals("OK", resp.getResponseBody());

        Timer timer = assertTimerPresent("/slow.method=GET.timer");

        long max = timer.getSnapshot().getMax();

        assertTrue("Actual max: " + max,
                Math.abs(MILLISECONDS.toNanos(SLEEP_TIME) - max) < MILLISECONDS.toNanos(100));
    }

    @Test
    public void testSlowAsyncResourceIsMeasuredAsSlow() throws ExecutionException, InterruptedException, IOException {
        Response resp = req("/slow/async");
        assertEquals(200, resp.getStatusCode());
        assertEquals("OK", resp.getResponseBody());

        Timer timer = assertTimerPresent("/slow/async.method=GET.timer");

        long max = timer.getSnapshot().getMax();

        assertTrue("Actual max: " + max,
                Math.abs(MILLISECONDS.toNanos(SLEEP_TIME) - max) < MILLISECONDS.toNanos(100));
    }

    private Timer assertTimerPresent(String name) throws InterruptedException {
        SortedMap<String, Timer> timers = waitForTimers(20);

        assertEquals(1, timers.size());

        Map.Entry<String, Timer> entry = timers.entrySet().iterator().next();
        assertEquals(name, entry.getKey());
        Timer timer = entry.getValue();
        assertEquals(1, timer.getCount());

        return timer;
    }

    /**
     * Spin waiting for the event to hit the metrics listener
     *
     * @param millis time to wait
     * @return timers map
     */
    @Nonnull
    private SortedMap<String, Timer> waitForTimers(long millis) throws InterruptedException {
        long start = System.nanoTime();

        SortedMap<String, Timer> timers;
        while (System.nanoTime() - start < MILLISECONDS.toNanos(millis)) {
            timers = registry.getTimers();
            if (!timers.isEmpty()) {
                return timers;
            }

            Thread.sleep(1);
        }

        throw new RuntimeException("Waited " + millis + "ms, did not get any timers");
    }

    @Override
    protected MetricArbiter getMetricArbiter() {
        return new FixedMetricArbiter(true, true);
    }

    @Override
    protected MetricNamer getMetricNamer() {
        return new RequestPathMetricNamer();
    }
}
