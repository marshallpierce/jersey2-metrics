package org.mpierce.jersey2.metrics;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import org.junit.Before;
import org.junit.Test;
import org.mpierce.metrics.reservoir.hdrhistogram.HdrHistogramReservoir;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

public class MetricProviderTest {

    private static final String NAME = "foo";
    private MetricProvider metricProvider;
    private MetricRegistry metricRegistry;

    @Before
    public void setUp() throws Exception {
        metricRegistry = new MetricRegistry();
        metricProvider = new MetricProvider(metricRegistry, HdrHistogramReservoir::new);
    }

    @Test
    public void testReturnsSameTimerForSameName() {
        Timer timer = metricProvider.getTimer(NAME);
        Timer timer2 = metricProvider.getTimer(NAME);

        assertSame(timer, timer2);
    }

    @Test
    public void testReturnsMetricRegistryTimerWhenAlreadyExists() {

        Timer timer = metricRegistry.timer(NAME);
        Timer timer2 = metricProvider.getTimer(NAME);

        assertSame(timer, timer2);
    }

    @Test
    public void testExplodesWhenTimerNameIsNotUsedForATimer() {
        metricRegistry.counter(NAME);
        try {
            metricProvider.getTimer(NAME);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals(NAME + " is already used for a different type of metric", e.getMessage());
        }
    }
}
