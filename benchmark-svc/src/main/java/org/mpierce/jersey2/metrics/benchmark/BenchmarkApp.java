package org.mpierce.jersey2.metrics.benchmark;

import org.glassfish.jersey.server.ResourceConfig;

public final class BenchmarkApp extends ResourceConfig {

    public BenchmarkApp(Object... components) {
        for (Object component : components) {
            register(component);
        }

        register(StaticStringResource.class);
        register(MultiResponseCodeResource.class);
    }
}
