package org.mpierce.jersey2.metrics.test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;

@Path("slow")
public class SlowResource {

    public static final int SLEEP_TIME = 1000;

    @GET
    public String get() throws InterruptedException {
        Thread.sleep(SLEEP_TIME);
        return "OK";
    }

    @GET
    @Path("async")
    public void get(@Suspended final AsyncResponse asyncResponse) throws InterruptedException {
        new Thread(() -> {
            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException e) {
                throw new RuntimeException("Interrupted", e);
            }
            asyncResponse.resume("OK");
        }).start();
    }
}
