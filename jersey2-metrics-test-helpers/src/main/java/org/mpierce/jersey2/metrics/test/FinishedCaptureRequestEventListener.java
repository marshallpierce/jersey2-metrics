package org.mpierce.jersey2.metrics.test;

import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;

import java.util.function.Consumer;

final class FinishedCaptureRequestEventListener implements RequestEventListener {

    private final Consumer<RequestEvent> requestEventConsumer;

    FinishedCaptureRequestEventListener(Consumer<RequestEvent> requestEventConsumer) {
        this.requestEventConsumer = requestEventConsumer;
    }

    @Override
    public void onEvent(RequestEvent event) {
        if (event.getType() == RequestEvent.Type.FINISHED) {
            requestEventConsumer.accept(event);
        }
    }
}
