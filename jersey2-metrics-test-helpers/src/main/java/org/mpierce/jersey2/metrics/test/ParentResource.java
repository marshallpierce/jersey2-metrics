package org.mpierce.jersey2.metrics.test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("parent")
public class ParentResource {

    @GET
    public String get() {
        return "parent";
    }
}
