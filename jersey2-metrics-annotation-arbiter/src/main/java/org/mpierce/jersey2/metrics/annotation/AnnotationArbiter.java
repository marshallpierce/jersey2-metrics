package org.mpierce.jersey2.metrics.annotation;

import org.glassfish.jersey.server.model.ResourceMethod;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.mpierce.jersey2.metrics.MetricArbiter;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.lang.reflect.Method;

/**
 * Arbiter that looks for methods or classes annotated with {@link ResourceMetrics}.
 */
@ThreadSafe
public final class AnnotationArbiter implements MetricArbiter {

    private static final MetricsAnnotationFeatureChecker TIMER_CHECKER = ResourceMetrics::timer;
    private static final MetricsAnnotationFeatureChecker STATUS_CODE_COUNTER_CHECKER =
        ResourceMetrics::statusCodeCounter;

    @Override
    public boolean shouldHaveTimer(@Nonnull RequestEvent event) {
        EnabledState state =
            getState(event.getUriInfo().getMatchedResourceMethod(), TIMER_CHECKER);
        return state == EnabledState.ON;
    }

    @Override
    public boolean shouldHaveStatusCodeCounter(@Nonnull RequestEvent event) {
        EnabledState state =
            getState(event.getUriInfo().getMatchedResourceMethod(), STATUS_CODE_COUNTER_CHECKER);
        return state == EnabledState.ON;
    }

    /**
     * Check the method, then the class, for @ResourceMetrics and use the specified checker to see if the feature is
     * enabled or disabled.
     *
     * @param resourceMethod resource method
     * @param checker        checker
     * @return EnabledState.ON if feature enabled, EnabledState.OFF if feature disabled, EnabledState.UNSPECIFIED
     * otherwise
     */
    static EnabledState getState(ResourceMethod resourceMethod, MetricsAnnotationFeatureChecker checker) {
        // check method, then class
        Method method = resourceMethod.getInvocable().getDefinitionMethod();
        for (ResourceMetrics ann : new ResourceMetrics[]{method.getAnnotation(ResourceMetrics.class),
            method.getDeclaringClass().getAnnotation(ResourceMetrics.class)}) {

            if (ann != null) {
                if (checker.check(ann)) {
                    return EnabledState.ON;
                } else {
                    return EnabledState.OFF;
                }
            }
        }

        return EnabledState.UNSPECIFIED;
    }
}
