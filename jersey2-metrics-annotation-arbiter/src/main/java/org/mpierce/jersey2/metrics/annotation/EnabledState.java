package org.mpierce.jersey2.metrics.annotation;

enum EnabledState {
    ON,
    OFF,
    UNSPECIFIED
}
