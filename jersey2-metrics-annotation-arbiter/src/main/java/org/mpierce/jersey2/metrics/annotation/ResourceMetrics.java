/*
 * Copyright (c) 2012 Palomino Labs, Inc.
 */

package org.mpierce.jersey2.metrics.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used with {@link org.mpierce.jersey2.metrics.annotation.AnnotationArbiter} to control metrics generation.
 *
 * This can be used on classes (to configure all methods in the class), methods, or both. Applying the annotation to a method overrides the class's annotation if both are present.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface ResourceMetrics {

    /**
     * @return true if timing should be measured for the annotated method (or all methods on the annotated class)
     */
    boolean timer() default true;

    /**
     * @return true if status codes be measured for the annotated method (or all methods on the annotated class)
     */
    boolean statusCodeCounter() default true;
}
